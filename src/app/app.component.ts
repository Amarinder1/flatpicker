import { Component } from '@angular/core';
import flatpickr from "flatpickr";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  date = '';

  onClick() {
    flatpickr(document.getElementById('test'), {
      enableTime: true,
      dateFormat: "Y-m-d H:i",
    });
  }
}
